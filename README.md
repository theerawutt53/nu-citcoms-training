# Citcoms Training
ณ ห้องปฏิบัติการคอมพิวเตอร์ 210 ชั้น 2 อาคาร CITCOMS


### หลักสูตร "Kubernetes (K8s)" 12-13 June, 2024
slide: 
https://www.canva.com/design/DAGHjuIlqhc/y-c3KbZLjpgbt9QT_Y2bOA/view
pdf: [download](./workshop-K8s/K8s.pdf)


### หลักสูตร "การพัฒนาเว็บไซต์แบบ Static และ Continuous Integration / Continuous Deployment (CI/CD) รุ่น 1" 14 June, 2024
slide: 
https://www.canva.com/design/DAGICfZ84ow/5Ip3RDBYU5QI7Fljs39XUA/view
pdf: [download](./workshop-cicd/CICD.pdf)

### หลักสูตร "การสร้างเว็บแอพพลิเคชั่นด้วย Node.JS" 17-19 June, 2024
slide: 
https://www.canva.com/design/DAGINwOypes/pu9-bkkx-HYOCAJDtz6z3A/view
pdf: [download](./workshop-cicd/CICD.pdf)