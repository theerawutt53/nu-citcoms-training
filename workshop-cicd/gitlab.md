# Command line instructions


### Git global setup
```sh
git config --global user.name "Theerawut Thaweephattharawong"
git config --global user.email "theerawutt53@gmail.com"
```

### Create a new repository
```sh
git clone https://gitlab.com/theerawutt53/nu-citcoms-training.git
cd nu-citcoms-training
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
```

### Push an existing folder
```sh
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/theerawutt53/nu-citcoms-training.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

### Push an existing Git repository
```sh
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/theerawutt53/nu-citcoms-training.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```