# Tools

* [Dokcer Hub](https://hub.docker.com)
* [Docker Desktop](https://code.visualstudio.com/download)
* [Visual Studio Code](https://code.visualstudio.com/download)
* [Nodejs](https://nodejs.org)
* [MongoDB](https://www.mongodb.com/try/download/community)


---

# ขั้นตอนติดตั้ง Docker Desktop

#### ให้เข้าไป download ตัว [installer]( https://www.mongodb.com/try/download/community) และติดตั้งตามขั้นตอนต่างๆ ดังนี้

1. ติดตั้ง Docker Desktop

![alt text](./images/installing-docker-desktop-1.png)
ขั้นตอนการติดตั้ง Docker Desktop


![alt text](./images/installing-docker-desktop-2.png)
ติดตั้ง Docker Desktop สำเร็จแล้ว

#### กด Accept เพื่อยอมรับ Docker Service Agreement

![alt text](./images/docker-welcome.png)