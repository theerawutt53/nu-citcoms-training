# การสร้าง Dockerfile สำหรับ NGINX เพื่อให้สามารถระบุ path ของเว็บได้

### ขั้นตอนการสร้าง Dockerfile สำหรับ NGINX ที่ให้บริการหลาย Web Application

### 1. สร้างไดเร็กทอรีสำหรับโปรเจกต์:

```sh
mkdir -p ~/my-nginx-app/src1
mkdir -p ~/my-nginx-app/src2
mkdir -p ~/my-nginx-app/conf.d
```

### 2. สร้างไฟล์ HTML สำหรับแต่ละ Web:

สร้างไฟล์ index.html ในไดเร็กทอรี src1:

```sh
echo "<h1>Hello from Path 1</h1>" > ~/my-nginx-app/src1/index.html
```


สร้างไฟล์ index.html ในไดเร็กทอรี src2:

```sh
echo "<h1>Hello from Path 2</h1>" > ~/my-nginx-app/src2/index.html
```

### 3. สร้างไฟล์การกำหนดค่า NGINX:

สร้างไฟล์การกำหนดค่า NGINX ในไดเร็กทอรี conf.d:
```sh
cat <<EOF > ~/my-nginx-app/conf.d/default.conf
server {
    listen 80;
    server_name localhost;

    location /{
        alias /usr/share/nginx/html/;
        index index.html;
    }

    location /path1/ {
        alias /usr/share/nginx/html/path1/;
        index index.html;
    }

    location /path2/ {
        alias /usr/share/nginx/html/path2/;
        index index.html;
    }
}
EOF
```
ในตัวอย่างนี้:
* เส้นทาง `/` ชี้ไปที่ `/usr/share/nginx/html/` สำหรับ root path
* เส้นทาง `/path1/` ชี้ไปที่ `/usr/share/nginx/html/path1/`
* เส้นทาง `/path2/` ชี้ไปที่ `/usr/share/nginx/html/path2/`

### 4. สร้าง Dockerfile สำหรับ NGINX:

สร้างไฟล์ `Dockerfile` ในไดเร็กทอรี `~/my-nginx-app`:

```sh
# Use the official NGINX base image
FROM nginx:latest

# Copy the content of your sites to the NGINX root directories
COPY src1/ /usr/share/nginx/html/path1/
COPY src2/ /usr/share/nginx/html/path2/

# Copy custom NGINX configuration file
COPY conf.d/default.conf /etc/nginx/conf.d/default.conf

# Expose port 80
EXPOSE 80
```

ในตัวอย่างนี้:

* ใช้ `nginx:latest` เป็น base image
* คัดลอกไฟล์เว็บจากไดเร็กทอรี `src1` ไปที่ `/usr/share/nginx/path1/` ใน container
* คัดลอกไฟล์เว็บจากไดเร็กทอรี `src2` ไปที่ `/usr/share/nginx/path2/` ใน container
* คัดลอกไฟล์การกำหนดค่า NGINX จากไดเร็กทอรี `conf.d` ไปที่ `/etc/nginx/conf.d/default.conf`
* เปิด port 80 เพื่อให้ container Listen Port นี้

### 5. สร้าง Docker image จาก Dockerfile:

ไปที่ไดเร็กทอรีที่มี Dockerfile แล้วรันคำสั่งต่อไปนี้:

```sh
cd ~/my-nginx-app
docker build -t my-nginx-app .
```

* `-t my-nginx-app`: ตั้งชื่อและ tag ให้กับ image (ในที่นี้คือ `my-nginx-app`)
* `.`: ระบุตำแหน่งของ Dockerfile (ในที่นี้คือ directory ปัจจุบัน)

ุ###  ุ6. รัน Docker container จาก image:

รัน Docker container โดยใช้ image ที่สร้างขึ้น:

```sh
docker run -d -p 8080:80 --name nginx-container my-nginx-app
```

* `-d`: รัน container ใน detached mode (ใน background)
* `-p 8080:80`: แมพ port 8080 บน host ไปที่ port 80 ใน container
* `--name nginx-container`: ตั้งชื่อให้กับ container
* `my-nginx-app`: ใช้ image ที่สร้างขึ้น

### 7. ตรวจสอบการทำงานของ container:

* เปิดเว็บเบราว์เซอร์แล้วไปที่ `http://localhost:8080/path1/` คุณควรจะเห็นข้อความ "Hello from Path 1"

* เปิดเว็บเบราว์เซอร์แล้วไปที่ `http://localhost:8080/path2/` คุณควรจะเห็นข้อความ "Hello from Path 2"