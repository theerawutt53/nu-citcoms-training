# Microsoft IIS Docker Container
### สร้าง Docker Image จาก Dockerfile

1. สร้างไฟล์ web และไดเร็กทอรีสำหรับเว็บของคุณ: \
สร้างไดเร็กทอรีสำหรับโปรเจกต์ของคุณและสร้างไฟล์ index.html ในไดเร็กทอรี src:
```sh
mkdir -p ~/my-iis-app/src
echo "<h1>Hello, IIS from Docker!</h1>" > ~/my-iis-app/src/index.html
```

2. สร้างไฟล์ Dockerfile ในไดเร็กทอรีของโปรเจกต์:
#### ตัวอย่าง Dockerfile สำหรับการตั้งค่า IIS บน Windows Server:
```sh
# Use the official Microsoft IIS base image
FROM mcr.microsoft.com/windows/servercore:ltsc2022

# Install IIS
RUN powershell -NoProfile -Command \
    Install-WindowsFeature -name Web-Server

# Copy the content of your site to the IIS root directory
COPY src/ /inetpub/wwwroot/

# Expose port 80
EXPOSE 80

# Start IIS
CMD ["powershell.exe", "Start-Service", "w3svc", ";", "Invoke-WebRequest", "-UseBasicParsing", "http://localhost"]
```
ในตัวอย่างนี้:

* ใช้ `mcr.microsoft.com/windows/servercore:ltsc2022` เป็น base image
* ติดตั้ง IIS โดยใช้ `Install-WindowsFeature -name Web-Server`
* คัดลอกไฟล์เว็บไปที่ `/inetpub/wwwroot/`
* เปิด port 80
* เริ่มบริการ IIS
<br><br>
3. สร้าง Docker image จาก Dockerfile:\
ไปที่ไดเร็กทอรีที่มี Dockerfile แล้วรันคำสั่งต่อไปนี้:
```sh
cd ~/my-iis-app
docker build -t my-iis-app .
```
* `-t my-iis-app`: ตั้งชื่อและ tag ให้กับ image (ในที่นี้คือ `my-iis-app`)
* `.`: ระบุตำแหน่งของ Dockerfile (ในที่นี้คือ directory ปัจจุบัน)

4. รัน Docker container จาก image:\
รัน Docker container โดยใช้ image ที่สร้างขึ้น:
```sh
docker run -d -p 8080:80 --name iis-container my-iis-app
```

* `-d`: รัน container ใน detached mode (ใน background)
* `-p 8080:80`: แมพ port 8080 บน host ไปที่ port 80 ใน container
* `--name iis-container`: ตั้งชื่อให้กับ container
`my-iis-app`: ใช้ image ที่สร้างขึ้น

5. ตรวจสอบการทำงานของ container:\
เปิดเว็บเบราว์เซอร์แล้วไปที่ http://localhost:8080 คุณควรจะเห็นข้อความ "Hello, IIS from Docker!"