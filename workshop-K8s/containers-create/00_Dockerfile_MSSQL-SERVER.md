# Microsoft SQL Server Docker Container
### สร้าง Docker Image จาก Dockerfile

1. สร้างไฟล์ setup.sql ในไดเร็กทอรีของโปรเจกต์ของคุณ:
```sh
CREATE DATABASE TestDB;
GO
USE TestDB;
CREATE TABLE Customers (ID INT, Name NVARCHAR(50));
INSERT INTO Customers (ID, Name) VALUES (1, 'John Doe');
GO
```
2. สร้างไฟล์ Dockerfile ในไดเร็กทอรีของโปรเจกต์ของคุณ:
##### ตัวอย่าง Dockerfile สำหรับ MSSQL
```sh
# ใช้ official MSSQL base image
FROM mcr.microsoft.com/mssql/server:2019-latest

# สร้าง directory สำหรับ script การตั้งค่า
RUN mkdir -p /usr/src/app

# คัดลอก script การตั้งค่าไปยัง container
COPY setup.sql /usr/src/app/setup.sql

# ตั้งค่าการรัน script การตั้งค่าหลังจาก MSSQL server เริ่มทำงาน
CMD /bin/bash -c '/opt/mssql/bin/sqlservr & sleep 30 && /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "YourPassword123!" -i /usr/src/app/setup.sql'
```

ในตัวอย่างนี้:

* ใช้ `mcr.microsoft.com/mssql/server:2019-latest` เป็น base image
* คัดลอกไฟล์ `setup.sql` ไปยัง directory `/usr/src/app` ใน container
* ตั้งค่าการรัน MSSQL server และรัน script การตั้งค่าหลังจาก MSSQL server เริ่มทำงาน

2.สร้าง Docker Image:
```sh
docker build -t my-mssql .
```
3. รัน Docker Container จาก Image ที่สร้างขึ้น:
```sh
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=YourPassword123!' -p 1433:1433 --name sqlserver-custom -d my-mssql
```