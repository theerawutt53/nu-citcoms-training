## ตัวอย่าง Dockerfile สำหรับแอปพลิเคชันเว็บ PHP:
การสร้าง Docker image จาก Dockerfile
```sh
# ใช้ base image เป็น php:7.4-apache
FROM php:7.4-apache

# คัดลอกไฟล์จากเครื่อง host ไปยัง directory ในคอนเทนเนอร์
COPY src/ /var/www/html/

# ติดตั้ง extensions ที่จำเป็นสำหรับ PHP
RUN docker-php-ext-install mysqli

# กำหนด port ที่คอนเทนเนอร์จะฟังการเชื่อมต่อ
EXPOSE 80

# กำหนดคำสั่งที่จะรันเมื่อคอนเทนเนอร์เริ่มทำงาน
CMD ["apache2-foreground"]
```

หลังจากสร้าง Dockerfile แล้ว เราสามารถสร้าง Docker image ได้ด้วยคำสั่ง:

```sh
docker build -t my-php-app .
```