# การสร้าง Dockerfile สำหรับ Nodejs Express

### ขั้นตอนการสร้างโปรเจกต์ Node.js ด้วย Express

### 1. สร้างไดเร็กทอรีสำหรับโปรเจกต์:
```sh
mkdir ~/my-node-app
cd ~/my-node-app
```

### 2. เริ่มโปรเจกต์ Node.js และติดตั้ง Express:
```sh
npm init -y
npm install express
```

### 3. สร้างไฟล์แอปพลิเคชัน Node.js (app.js):

สร้างไฟล์ `app.js` ในไดเร็กทอรี `~/my-node-app`:
```js
const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

// Serve static files from the "public" directory
app.use(express.static(path.join(__dirname, 'public')));

// Define a route for the home page
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
```
### 4. สร้างไดเร็กทอรีสำหรับไฟล์ HTML:
```sh
mkdir public
```

### 5. สร้างไฟล์ HTML (index.html):
สร้างไฟล์ index.html ในไดเร็กทอรี public:
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Node.js Express App</title>
</head>
<body>
    <h1>Hello from Node.js and Express!</h1>
</body>
</html>
```

### 5. สร้าง Dockerfile สำหรับแอปพลิเคชัน Node.js
สร้างไฟล์ `Dockerfile` ในไดเร็กทอรี `~/my-node-app:`
```sh
# Use the official Node.js base image
FROM node:16

# Set the working directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the application port
EXPOSE 3000

# Start the application
CMD ["node", "app.js"]

```
ในตัวอย่างนี้:

* ใช้ `node:14` เป็น base image
* ตั้งค่าไดเร็กทอรีทำงานเป็น `/usr/src/app`
* คัดลอกไฟล์ `package.json` และ `package-lock.json` และติดตั้ง dependencies
* คัดลอกโค้ดแอปพลิเคชันที่เหลือไปยัง container
* เปิด port 3000 เพื่อให้ container ฟังการเชื่อมต่อที่ port นี้
* ตั้งค่าให้เริ่มต้นแอปพลิเคชันด้วย `node app.js`

### 5. สร้าง Docker Image จาก Dockerfile:
ไปที่ไดเร็กทอรีที่มี Dockerfile แล้วรันคำสั่งต่อไปนี้:
```sh
docker build -t my-node-app .
```
* `-t my-node-app`: ตั้งชื่อและ tag ให้กับ image (ในที่นี้คือ `my-node-app`)
* `.`: ระบุตำแหน่งของ Dockerfile (ในที่นี้คือ directory ปัจจุบัน)

### 4. รัน Docker Container จาก Image ที่สร้างขึ้น:
```sh
docker run -d -p 3000:3000 --name node-app-container my-node-app
```
* `-d`: รัน container ใน detached mode (ใน background)
* `-p 3000:3000`: แมพ port 3000 บน host ไปที่ port 3000 ใน container
* `--name node-app-container`: ตั้งชื่อให้กับ container
* `my-node-app`: ใช้ image ที่สร้างขึ้น

### 4. ตรวจสอบการทำงานของ Container:
* เปิดเว็บเบราว์เซอร์แล้วไปที่ `http://localhost:3000` คุณควรจะเห็นข้อความ "Hello from Node.js and Express!"