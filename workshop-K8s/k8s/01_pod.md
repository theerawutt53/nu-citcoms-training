# Kubernetes Objects: POD
#### ตัวอย่างการใช้งาน Pod
นี่คือตัวอย่างไฟล์ YAML ที่กำหนดค่า Pod เพื่อรันแอปพลิเคชันเว็บที่มีคอนเทนเนอร์หนึ่งตัว:

```sh
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: nginx:latest
    ports:
    - containerPort: 80
```

คำอธิบายของไฟล์ YAML
* `apiVersion`: ระบุเวอร์ชันของ Kubernetes API ที่ใช้งาน (`v1`)
* `kind`: ระบุชนิดของ Object (`Pod`)
* `metadata`: ข้อมูลเมทาดาทาที่เกี่ยวข้องกับ Pod เช่น ชื่อ (`name: myapp-pod`)` และป้ายกำกับ (`labels: app: myapp`)
* `spec`: กำหนดสเปคหรือข้อกำหนดของ Pod
* `containers`: รายการคอนเทนเนอร์ที่อยู่ใน Pod
* `name`: ชื่อของคอนเทนเนอร์ (`myapp-container`)
* `image`: อิมเมจของคอนเทนเนอร์ที่จะรัน (`nginx:latest`)
* `ports`: พอร์ตที่คอนเทนเนอร์จะใช้ (`containerPort: 80`)

#### การสร้าง Pod
คุณสามารถสร้าง Pod จากไฟล์ YAML นี้โดยใช้คำสั่ง kubectl:

```sh
kubectl apply -f pod.yaml
```

#### การตรวจสอบสถานะของ Pod
คุณสามารถตรวจสอบสถานะของ Pod ที่สร้างขึ้นโดยใช้คำสั่ง:

```sh
kubectl get pods
```

และถ้าต้องการดูรายละเอียดเพิ่มเติม:

```sh
kubectl describe pod myapp-pod

```