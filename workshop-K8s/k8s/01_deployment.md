# Kubernetes Objects: Deployment
#### ตัวอย่างการใช้งาน Deployment
นี่คือตัวอย่างไฟล์ YAML ที่กำหนดค่า Deployment เพื่อรันแอปพลิเคชันเว็บที่มีคอนเทนเนอร์หนึ่งตัวและกำหนดให้มี 3 replicas:

```sh
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
  labels:
    app: myapp
spec:
  replicas: 3
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - name: myapp-container
        image: nginx:latest
        ports:
        - containerPort: 80
```

คำอธิบายของไฟล์ YAML
* `apiVersion`: ระบุเวอร์ชันของ Kubernetes API ที่ใช้งาน (`apps/v1`)
* `kind`: ระบุชนิดของ Object (`Deployment`)
* `metadata`: ข้อมูลเมทาดาทาที่เกี่ยวข้องกับ Deployment เช่น ชื่อ (`name: myapp-deployment`) และป้ายกำกับ (`labels: app: myapp`)
* `spec`: กำหนดสเปคหรือข้อกำหนดของ Deployment
* `replicas`: จำนวน Pod ที่ต้องการ (`replicas: 3`)
* `selector`: กำหนดเงื่อนไขในการเลือก Pod ที่จะจัดการ (`matchLabels: app: myapp`)
* `template`: กำหนดแม่แบบสำหรับ Pod ที่จะสร้าง
* `metadata`: ข้อมูลเมทาดาทาของ Pod
* `spec`: สเปคของ Pod
* `containers`: รายการคอนเทนเนอร์ที่อยู่ใน Pod
* `name`: ชื่อของคอนเทนเนอร์ (`myapp-container`)
* `image`: อิมเมจของคอนเทนเนอร์ที่จะรัน (`nginx:latest`)
* `ports`: พอร์ตที่คอนเทนเนอร์จะใช้ (`containerPort: 80`)

#### การสร้าง Deployment
คุณสามารถสร้าง Deployment จากไฟล์ YAML นี้โดยใช้คำสั่ง `kubectl`:

```sh
kubectl apply -f deployment.yaml
```

#### การตรวจสอบสถานะของ Deployment
คุณสามารถตรวจสอบสถานะของ Deployment ที่สร้างขึ้นโดยใช้คำสั่ง:

```sh
kubectl get deployments
```

และถ้าต้องการดูรายละเอียดเพิ่มเติม:

```sh
kubectl describe deployment myapp-deployment
```

#### การปรับขนาด Deployment
คุณสามารถปรับขนาดจำนวน Pod ใน Deployment โดยใช้คำสั่ง:
```sh
kubectl scale deployment myapp-deployment --replicas=5
```

#### การอัปเดต Deployment
คุณสามารถอัปเดต Deployment โดยการแก้ไขไฟล์ YAML แล้วใช้คำสั่ง `apply` อีกครั้ง:
```sh
kubectl apply -f deployment.yaml
```
หรือใช้คำสั่ง set image เพื่ออัปเดตอิมเมจของคอนเทนเนอร์:
```sh
kubectl set image deployment/myapp-deployment myapp-container=nginx:1.19.1
```

#### การย้อนกลับ Deployment
หากมีปัญหาหลังจากอัปเดต Deployment คุณสามารถย้อนกลับไปยังเวอร์ชันก่อนหน้าได้:
```sh
kubectl rollout undo deployment/myapp-deployment
```