# การใช้งาน StatefulSet กับ MongoDB

การใช้งาน StatefulSet กับ MongoDB เป็นตัวอย่างที่ดีในการแสดงถึงการใช้งาน StatefulSet สำหรับแอปพลิเคชันที่ต้องการการเก็บสถานะ (stateful application) เนื่องจาก MongoDB เป็นฐานข้อมูลที่ต้องการการเก็บข้อมูลที่ถาวรและการจัดการที่เฉพาะเจาะจง

### ตัวอย่างการใช้งาน StatefulSet กับ MongoDB

#### 1. สร้าง ConfigMap สำหรับการกำหนดค่า MongoDB
```sh
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongodb-config
  labels:
    app: mongodb
data:
  mongod.conf: |
    storage:
      dbPath: /data/db
    net:
      bindIp: 0.0.0.0
    replication:
      replSetName: rs0
```

#### 2. สร้าง StatefulSet สำหรับ MongoDB
```sh
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongodb
spec:
  serviceName: "mongodb"
  replicas: 3
  selector:
    matchLabels:
      app: mongodb
  template:
    metadata:
      labels:
        app: mongodb
    spec:
      containers:
      - name: mongodb
        image: mongo:4.4.6
        ports:
        - containerPort: 27017
        volumeMounts:
        - name: mongodb-data
          mountPath: /data/db
        - name: mongodb-config
          mountPath: /data/configdb
          subPath: mongod.conf
  volumeClaimTemplates:
  - metadata:
      name: mongodb-data
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 5Gi
```

#### 3. สร้าง Headless Service เพื่อใช้สำหรับการค้นหาและเข้าถึง Pod แต่ละตัว
```sh
apiVersion: v1
kind: Service
metadata:
  name: mongodb
spec:
  clusterIP: None
  selector:
    app: mongodb
  ports:
    - port: 27017
      targetPort: 27017
```

คำอธิบาย
* `ConfigMap`: ใช้เพื่อกำหนดค่า MongoDB ซึ่งมีการตั้งค่าต่างๆ เช่น `dbPath`, `bindIp`, และ `replication`
* `StatefulSet`: ประกอบด้วยส่วนประกอบต่อไปนี้:
* `serviceName`: ชื่อของ Service ที่ใช้ในการควบคุมการเข้าถึง Pod
* `replicas`: จำนวน Pod ที่ต้องการ (`3`)
* `selector`: เลือก Pod ที่ตรงกับป้ายกำกับ (`app: mongodb`)
* `template`: แม่แบบสำหรับ Pod ที่จะสร้าง
* `containers`: กำหนดคอนเทนเนอร์ MongoDB ที่จะรัน รวมถึงการกำหนด volumeMounts สำหรับเก็บข้อมูลและการกำหนดค่า
* `volumeClaimTemplates`: สร้าง PersistentVolumeClaim สำหรับแต่ละ Pod เพื่อเก็บข้อมูลถาวร
* `Headless Service`: ใช้เพื่อให้สามารถค้นหา Pod โดยใช้ DNS และไม่มีการใช้ ClusterIP (`clusterIP: None`)


#### การสร้างและตรวจสอบ
สามารถสร้าง ConfigMap, StatefulSet, และ Service โดยใช้คำสั่ง `kubectl`:
```sh
kubectl apply -f mongodb-config.yaml
kubectl apply -f mongodb-statefulset.yaml
kubectl apply -f mongodb-service.yaml
```

#### การตรวจสอบสถานะ
สามารถตรวจสอบสถานะของ StatefulSet และ Pod ที่สร้างขึ้นโดยใช้คำสั่ง:
```sh
kubectl get statefulsets
kubectl get pods
kubectl get services
```


#### การตั้งค่า Replica Set ของ MongoDB
หลังจากที่ StatefulSet ของ MongoDB ถูกสร้างขึ้นแล้ว คุณจะต้องตั้งค่า Replica Set สำหรับ MongoDB โดยการเข้าสู่ MongoDB shell ในหนึ่งใน Pod และรันคำสั่งดังนี้:
1. เข้าสู่ Pod:
```sh
kubectl exec -it mongodb-0 -- mongo
```

2 รันคำสั่งเพื่อสร้าง Replica Set:
```sh
rs.initiate({
  _id: "rs0",
  members: [
    { _id: 0, host: "mongodb-0.mongodb:27017" },
    { _id: 1, host: "mongodb-1.mongodb:27017" },
    { _id: 2, host: "mongodb-2.mongodb:27017" }
  ]
})
```
