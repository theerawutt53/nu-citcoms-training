# Kubernetes Objects: Service
#### ตัวอย่างการใช้งาน Service
นี่คือตัวอย่างไฟล์ YAML ที่กำหนดค่า Service เพื่อเปิดเผยแอปพลิเคชันเว็บที่รันใน Pod:

```sh
apiVersion: v1
kind: Service
metadata:
  name: myapp-service
spec:
  selector:
    app: myapp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer
```

คำอธิบายของไฟล์ YAML
* `apiVersion`: ระบุเวอร์ชันของ Kubernetes API ที่ใช้งาน (v1)
* `kind`: ระบุชนิดของ Object (`Service`)
* `metadata`: ข้อมูลเมทาดาทาที่เกี่ยวข้องกับ Service เช่น ชื่อ (`name: myapp-service`)
* `spec`: กำหนดสเปคหรือข้อกำหนดของ Service
* `selector`: ระบุป้ายกำกับ (labels) ที่ใช้ในการเลือก Pod ที่จะบริการ (`app: myapp`)
* `ports`: รายการพอร์ตที่เปิดเผยใน Service
* `protocol`: โปรโตคอลที่ใช้ (`TCP`)
* `port`: พอร์ตที่เปิดเผยใน Service (`80`)
* `targetPort`: พอร์ตที่ Pod ใช้ (`80`)
* `type`: ประเภทของ Service (`LoadBalancer`)

## Type of Service
* `ClusterIP` (default) : ให้บริการภายใน cluster เท่านั้น  (internal IP) ไม่สามารถเข้าถึงจากภายนอกได้

* `NodePort`: เปิดพอร์ตบนทุก node เพื่อให้สามารถเข้าถึง service ผ่าน IP ของ node โดยใช้  <NodeIP>:<NodePort> ทำให้สามารถเข้าถึงจากภายนอก Cluster ได้โดยใช้ **ไม่ใช่ทางเลือกที่ดี สำหรับการรับส่งข้อมูลจากภายนอก**

* `LoadBalancer`: สร้าง load balancer ภายนอกและกระจาย ให้รับส่งข้อมูลจากภายนอกไปยัง pods ที่อยู่ภายใน cluster ซึ่งเหมาะสำหรับการรับส่งข้อมูลจากภายนอกจริงๆ อันนี้จะใช้ร่วมกับ Cloud Provider

* `ExternalName`: ใช้ในการอ้างอิง service ภายนอก cluster โดยใช้ DNS

* `Headless`: ไม่มี ClusterIP และให้การเชื่อมต่อโดยตรงไปยัง pods

#### การสร้าง Service
คุณสามารถสร้าง Service จากไฟล์ YAML นี้โดยใช้คำสั่ง kubectl:

```sh
kubectl apply -f service.yaml
```

#### การตรวจสอบสถานะของ Service
คุณสามารถตรวจสอบสถานะของ Service ที่สร้างขึ้นโดยใช้คำสั่ง:

```sh
kubectl get services
```

และถ้าต้องการดูรายละเอียดเพิ่มเติม:

```sh
kubectl describe service myapp-service
```

### ตัวอย่างเพิ่มเติมของประเภท Service
#### 1. ClusterIP

```sh
apiVersion: v1
kind: Service
metadata:
  name: myapp-clusterip
spec:
  selector:
    app: myapp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: ClusterIP

```

#### 2. NodePort

```sh
apiVersion: v1
kind: Service
metadata:
  name: myapp-nodeport
spec:
  selector:
    app: myapp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30036
  type: NodePort
```


#### 3. ExternalName
```sh
apiVersion: v1
kind: Service
metadata:
  name: myapp-externalname
spec:
  type: ExternalName
  externalName: example.com

```