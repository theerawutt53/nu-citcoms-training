curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

kubectl create namespace gitlab-runner

helm repo add gitlab https://charts.gitlab.io
helm repo update


helm install --namespace gitlab-runner gitlab-runner -f gitlab-runner.yaml gitlab/gitlab-runner

kubectl logs -n kube-system pod/helm-install-gitlab-runner