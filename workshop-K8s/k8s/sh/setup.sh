sudo apt-get update

#Initial cluster
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.24.9+k3s1 INSTALL_K3S_EXEC="--token [create XXXXXXX]" sh -s - server --cluster-init

# Join Master   
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION='v1.24.9+k3s1' INSTALL_K3S_EXEC="--write-kubeconfig-mode 644 --token XXXXXXXXXXX" sh -s - server --server https://:6443 

# Join Agent
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION='v1.24.9+k3s1' K3S_URL=https://:6443 K3S_TOKEN=XXX sh -

# Check k3s Active
kubectl config view --flatten=true

#Gitlab Token
echo -n "username:personal-access-token" | base64

mkdir -p /etc/rancher/k3s/ &&
nano /etc/rancher/k3s/registries.yaml

mirrors:
  registry.gitlab.com:
    endpoint:
      - "https://registry.gitlab.com"

configs:
  "registry.gitlab.com":
    auth:
      auth: "XXX"

# ---------------------------------------------------------------------------------------------

systemctl restart k3s
systemctl status k3s
systemctl restart k3s-agent
systemctl status k3s-agent

kubectl get nodes --show-labels

# ---------------------------------------------------------------------------------------------

kubectl label nodes node-19 mongodb-rs=rs6

kubectl --context do label nodes node-06 mongodb-rs=rs1


~/.kube/config

# Get Token
cat /var/lib/rancher/k3s/server/node-token