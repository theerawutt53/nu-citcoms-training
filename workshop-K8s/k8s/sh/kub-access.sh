sudo k3s kubectl cluster-info

export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

kubectl version --short

kubectl get nodes
