# Setting up Kind.

```
curl.exe -Lo kind-windows-amd64.exe https://kind.sigs.k8s.io/dl/v0.20.0/kind-windows-amd64

Move-Item .\kind-windows-amd64.exe C:\k8s\kind.exe
```


On windows find for `“Advanced system settings”` > Click on `“Environment Variables”` under `“System Variables”` set new `“Path”` variable to `C:\k8s`

### สร้าง Kubernetes Cluster
```sh
kind create cluster
```
ดู container status
```sh
docker ps
```
เราจะพบว่ามี container สร้างขึ้นมาใหม่ 1 ตัว คือ kind-control-plane ทำหน้าที่เป็น master node แต่ยังไม่มี worker node

```sh
CONTAINER ID   IMAGE                    COMMAND                  CREATED          STATUS          PORTS                       NAMES
3cb0473dd019   kindest/node:v1.21.1     "/usr/local/bin/entr…"   3 minutes ago    Up 3 minutes    127.0.0.1:60058->6443/tcp   kind-control-plane
```

### สร้าง Cluster แบบ ระบุชื่อ 
```sh
kind create cluster --name demo-cluster
```

เราจะได้ `cluster` ตัวที่ 2 ซึ่งมีชื่อว่า `demo-cluster` 

```sh
docker ps
```
เรามี container เพิ่มขึ้นมาอีก 1 ตัวคือ demo-cluster-control-plane (ชื่อ cluster ตามด้วยคำว่า control-plane)

```sh
CONTAINER ID   IMAGE                    COMMAND                  CREATED          STATUS          PORTS                       NAMES
610fc9255a20   kindest/node:v1.21.1     "/usr/local/bin/entr…"   29 seconds ago   Up 26 seconds   127.0.0.1:60083->6443/tcp   demo-cluster-control-plane
3cb0473dd019   kindest/node:v1.21.1     "/usr/local/bin/entr…"   3 minutes ago    Up 3 minutes    127.0.0.1:60058->6443/tcp   kind-control-plane
```

### list cluster
```sh
kind get clusters
```
ตอนนี้เรามี cluster อยู่ 2 cluster คือ

```sh
demo-cluster
kind
```

### ติดต่อกับ cluster
เมื่อเรา run คำสั่ง kubectl แล้วจะทำที่ cluster ที่ถูกสร้างขึ้นมาหลังสุด
```sh
kubectl config use-context kind-[ชื่อ cluster]
```

### ลบ cluster

```sh
kind delete cluster
```
หรือ
```sh
kind delete cluster --name demo-cluster
```

### Kind Configuration
เราสามารถกำหนดจำนวนของ node ใน cluster ได้ด้วยการเขียน configuration file ในตัวอย่างนี้ผมจะตั้งชื่อไฟล์ว่า `kind-config.yaml`

```sh
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
    - role: control-plane
    - role: worker
    - role: worker
```

### สร้าง cluster โดยที่ระบุ configuration file
```sh
kind create cluster --config kind-config.yaml
```

### สร้าง cluster โดยที่ระบุ configuration file และระบุชื่อ cluster
```sh
kind create cluster --config kind-config.yaml --name my-cluster
```

### ดูรายละเอียด cluster 
```sh
kubectl cluster-info --context kind-my-cluster
```