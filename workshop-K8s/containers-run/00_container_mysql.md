
# MySQL Docker Container

This project demonstrates how to run an MySQL using Docker.

## Prerequisites

- Docker installed on your machine. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Getting Started

### Step 1: Pull the MySQL  Docker Image

Open your terminal or command prompt and execute the following command to pull the latest MySQL image from Docker Hub:

```sh
docker pull mysql:latest
```

### Step 2: Run a MySQL Container:
Once the image is pulled, you can run a MySQL container. Use the following command to start the container:

```sh
docker run --name mysql-container -e MYSQL_ROOT_PASSWORD=my-secret-pw -p 3306:3306 -d mysql:latest
```

#### คำอธิบาย:

* `docker run` เป็นคำสั่งในการรัน container
* `--name mysql-container` : Names the container mysql-container (you can choose any name you prefer).
* `-e MYSQL_ROOT_PASSWORD=my-secret-pw`: Sets the environment variable for the root password of MySQL (replace my-secret-pw with your desired password).
* `-p 3306:3306`: Maps port 3306 on your host to port 3306 on the container.
* `-d` ทำการรัน container ในโหมด detach (เบื้องหลัง)
* `mysql:latest`: Specifies the image to use (you can replace latest with a specific version tag if needed).

### Step 3: Manage Your Docker Container
Here are some useful Docker commands for managing your container:

* View running containers:
```sh
docker ps
```

* View container logs:
```sh
docker logs mysql-container
```

* Access to the container:
```sh
docker exec -it mysql-container mysql -uroot -p
#cd /var/lib/mysql
```
* `-it`: Allows you to interact with the container.
* `mysql-container`: The name of your running MySQL container.
* `mysql -uroot -p`: Starts a MySQL client and connects to the server as the root user. It will prompt you for the root password you set earlier.

* Stop the container:
```sh
docker stop mysql-container
```

* Start the container:
```sh
docker start mysql-container
```

* Remove the container:
```sh
docker rm my-nginx
```

### Step 4: Persisting Data (Optional):
If you want to persist MySQL data outside of the container, you can mount a volume. Here’s an example command to run a MySQL container with a volume:

```sh
docker run --name mysql-container -e MYSQL_ROOT_PASSWORD=my-secret-pw -v /my/own/datadir:/var/lib/mysql -d mysql:latest
```
* `-v /my/own/datadir:/var/lib/mysql`: Mounts the host directory `/my/own/datadir` to the container’s MySQL data directory /var/lib/mysql.