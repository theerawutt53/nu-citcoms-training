
# Nginx Docker Container

This project demonstrates how to run an Nginx web server using Docker.

## Prerequisites

- Docker installed on your machine. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Getting Started

### Step 1: Pull the Nginx Docker Image

Pull the latest Nginx image from Docker Hub:

```sh
docker pull nginx
```

### Step 2: Run the Docker Container
Run an Nginx container with the following command:

```sh
docker run --name nginx-container -p 80:80 -d nginx
```

#### คำอธิบาย:

* `docker run` เป็นคำสั่งในการรัน container
* `--name nginx-container` ตั้งชื่อให้กับ container ว่า nginx-container
* `-p 80:80` ทำการแมปพอร์ต 80 ของ container กับพอร์ต 80 ของเครื่องเรา
* `-d` ทำการรัน container ในโหมด detach (เบื้องหลัง)
* `nginx` เป็นชื่อของ image ที่เราต้องการใช้

### Step 3: Manage Your Docker Container
Here are some useful Docker commands for managing your container:

* View running containers:
```sh
docker ps
```

* View container logs:
```sh
docker logs nginx-container
```

* Stop the container:
```sh
docker stop nginx-container
```

* Start the container:
```sh
docker start nginx-container
```

* Remove the container:
```sh
docker rm nginx-container
```

* Access to the container:
```sh
docker exec -it nginx-container /bin/bash

#cd /usr/share/nginx/html
#nano /etc/nginx/nginx.conf
```

### Step 4: Persisting Data (Optional):
If you want to persist Nginx data outside of the container, you can mount a volume. Here’s an example command to run a Nginx container with a volume:

```sh
docker run --name nginx-container -p 80:80 -v /web:/usr/share/nginx/html -d nginx

```
* `-v /web:/usr/share/nginx/html`: Mounts the host directory `/web` to the container’s Nginx HTML directory /usr/share/nginx/html.