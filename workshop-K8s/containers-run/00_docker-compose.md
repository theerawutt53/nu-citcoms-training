# Docker Compose
การสร้าง Dockerfile สำหรับ WordPress ทำได้โดยการใช้ฐานข้อมูล MySQL ร่วมกับภาพ Docker ที่มี WordPress ที่เตรียมไว้แล้วบน Docker Hub และการตั้งค่าเพิ่มเติมตามที่ต้องการ

### โครงสร้างของไฟล์ docker-compose.yml
ไฟล์ docker-compose.yml ประกอบด้วยการกำหนดคอนเทนเนอร์หลายๆ คอนเทนเนอร์ที่ประกอบขึ้นเป็นแอปพลิเคชัน ซึ่งสามารถกำหนดบริการต่างๆ เช่น เว็บเซิร์ฟเวอร์ ฐานข้อมูล แคช และอื่นๆ ได้

นี่คือตัวอย่างของไฟล์ docker-compose.yml สำหรับการรัน WordPress กับ MySQL:
```sh
version: '3.8'

services:
  db:
    image: mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: example_password
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress_password

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress_password
      WORDPRESS_DB_NAME: wordpress

volumes:
  db_data:
```

ในตัวอย่างนี้:

* `version`: กำหนดเวอร์ชันของ Docker Compose ที่ใช้
* `services`: กำหนดบริการต่างๆ ที่ประกอบขึ้นเป็นแอปพลิเคชัน
* `db`: บริการฐานข้อมูล MySQL
* `image`: กำหนดภาพ Docker ที่ใช้
* `volumes`: กำหนดการเก็บข้อมูลของ MySQL บนโฮสต์
* `restart`: กำหนดให้คอนเทนเนอร์เริ่มใหม่เสมอเมื่อเกิดข้อผิดพลาด
* `environment`: กำหนดตัวแปรสภาพแวดล้อมสำหรับการตั้งค่าฐานข้อมูล
* `wordpress`: บริการ WordPress
* `depends_on`: กำหนดให้บริการนี้เริ่มทำงานหลังจาก `db` เริ่มทำงาน
* `image`: กำหนดภาพ Docker ที่ใช้
* `ports`: กำหนดการแมพพอร์ต
* `restart`: กำหนดให้คอนเทนเนอร์เริ่มใหม่เสมอเมื่อเกิดข้อผิดพลาด
* `environment`: กำหนดตัวแปรสภาพแวดล้อมสำหรับการตั้งค่า WordPress
* `volumes`: กำหนดโวลุ่มสำหรับการเก็บข้อมูลของบริการต่างๆ

การใช้งาน Docker Compose\
1. รันคอนเทนเนอร์ทั้งหมด:
```sh
docker-compose up
```

การใช้ -d เพื่อรันในโหมด detached:
```sh
docker-compose up -d
```

2. หยุดคอนเทนเนอร์ทั้งหมด:
```sh
docker-compose down
```

3. ดูสถานะคอนเทนเนอร์ทั้งหมด:
```sh
docker-compose ps
```

3. ดู logs คอนเทนเนอร์ทั้งหมด:
```sh
docker-compose logs
```