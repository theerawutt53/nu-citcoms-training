
# PHP Docker Container

To run a PHP application in a Docker container

## Prerequisites

- Docker installed on your machine. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Getting Started

### Step 1: Pull the PHP Docker Image:

Open your terminal or command prompt and execute the following command to pull the latest PHP image from Docker Hub:

```sh
docker pull php:latest
```

### Step 2: Create a Directory for Your PHP Application:
Create a directory on your host machine for your PHP application. For example:
```sh
mkdir -p ~/my-php-app
cd ~/my-php-app
```
### Step 3: Add a PHP File:
Create a simple PHP file in the directory. For example, create an index.php file with the following content:
```sh
<?php
  phpinfo();
?>
```


### Step 4: Run a PHP Container with Volume Mount:
Use the `-v` flag to mount your PHP application directory to the appropriate location inside the container. Here’s an example command:

```sh
docker run --name php-container -d -p 8080:80 -v ~/my-php-app:/var/www/html php:latest
```

#### คำอธิบาย:

* `--name php-container`: Names the container php-container (you can choose any name you prefer).
* `-d`: Runs the container in detached mode (in the background).
* `-p 8080:80`: Maps port 8080 on your host to port 80 in the container.
* `-v ~/my-php-app:/var/www/html`: Mounts the host directory `~/my-php-app` to `/var/www/html` in the container, which is the default document root for PHP in the container.
* `php:latest`: Specifies the PHP image to use (you can replace latest with a specific version tag if needed).

### Step 5: Manage Your Docker Container
Verify that the container is running:

* View running containers:
```sh
docker ps
```

* View container logs:
```sh
docker logs php-container
```

* Stop the container:
```sh
docker stop php-container
```

* Start the container:
```sh
docker start php-container
```

* Remove the container:
```sh
docker rm php-container
```

* Access to the container:
```sh
docker exec -it php-container /bin/bash
```

### Step 6: Access Your PHP Application:
Open a web browser and go to http://localhost:8080. You should see the PHP information page, confirming that PHP is running and serving your application.


### Step 7: Use a Custom PHP Configuration (Optional)
If you need to use a custom PHP configuration, you can create a php.ini file in your project directory and mount it to the container. Here’s how to do it:
```sh
mkdir -p ~/my-php-app/config
```
Add your custom settings to ~/my-php-app/config/php.ini. Then run the container with an additional volume mount for the php.ini file:

```sh
docker run --name php-container -d -p 8080:80 -v ~/my-php-app:/var/www/html -v ~/my-php-app/config/php.ini:/usr/local/etc/php/php.ini php:latest
```