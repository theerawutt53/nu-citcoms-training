
# MongoDB Docker Container

This project demonstrates how to run an MySQL using Docker.

## Prerequisites

- Docker installed on your machine. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Getting Started

### Step 1: Pull the MongoDB  Docker Image

Open your terminal or command prompt and execute the following command to pull the latest MongoDB image from Docker Hub:

```sh
docker pull mongo:latest
```

### Step 2: Run a MongoDB Container:
Use the following command to start a MongoDB container:

```sh
docker run --name mongodb-container -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password mongo:latest
```

#### คำอธิบาย:

* `docker run` เป็นคำสั่งในการรัน container
* `--name mongodb-container`: Names the container `mongodb-container` (you can choose any name you prefer).
* `-d`: Runs the container in detached mode (in the background).
* `-p 27017:27017`: Maps port 27017 on your host to port 27017 in the container (MongoDB's default port).
* `-e MONGO_INITDB_ROOT_USERNAME=admin`: Sets the environment variable for the root username.
* `-e MONGO_INITDB_ROOT_PASSWORD=password`: Sets the environment variable for the root password.
* `mongo:latest`: Specifies the image to use (you can replace latest with a specific version tag if needed).
### Step 3: Manage Your Docker Container
Here are some useful Docker commands for managing your container:

* View running containers:
```sh
docker ps
```

* View container logs:
```sh
docker logs mongodb-container
```

* Access to the container:
```sh
docker exec -it mongodb-container /bin/bash
```

* Connecting to MongoDB:
You can now connect to the MongoDB server running in the Docker container using the mongo shell or any MongoDB client. For example, using the mongo shell:
```sh
mongo --host 127.0.0.1 --port 27017 -u admin -p password --authenticationDatabase admin
```

* `--host 127.0.0.1`: Specifies the host (localhost).
* `--port 27017`: Specifies the port (27017).
* `-u admin`: Specifies the MongoDB root user.
* `-p password`: Specifies the password for the root user.
* `--authenticationDatabase admin`: Specifies the authentication database (usually `admin`).

* Stop the container:
```sh
docker stop mongodb-container
```

* Start the container:
```sh
docker start mongodb-container
```

* Remove the container:
```sh
docker rm mongodb-container
```

### Step 4: Persisting Data (Optional):
If you want to persist MongoDB data outside of the container, you can mount a volume. Here’s an example command to run a MongoDB container with a volume:

```sh
docker run --name mongodb-container -d -p 27017:27017 -v ~/mongodb/data:/data/db -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password mongo:latest
```
* `-v ~/mongodb/data:/data/db`: Mounts the host directory `~/mongodb/data` to the container’s MongoDB data directory `/data/db`.