
# WordPress Docker Container

This project demonstrates how to run an WordPress using Docker.

## Prerequisites

- Docker installed on your machine. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Getting Started

### Step 1: Pull the WordPress Docker Image

Pull the latest WordPress image from Docker Hub:

```sh
docker pull wordpress:latest
```

### Step 2: Run the Docker Container
Run an WordPress container with the following command:

```sh
docker run -d --name wordpress-container \
  -e WORDPRESS_DB_HOST=mysql_host \
  -e WORDPRESS_DB_USER=mysql_user \
  -e WORDPRESS_DB_PASSWORD=mysql_password \
  -e WORDPRESS_DB_NAME=mysql_database \
  -p 8080:80 \
  wordpress
```

#### คำอธิบาย:

* `-d`: ระบุให้ container ทำงานในโหมด detached
* `--name wordpress-container`: กำหนดชื่อของ container เป็น wordpress-container
* `-e WORDPRESS_DB_HOST=mysql_host`: กำหนด host ของ MySQL database server ให้กับ WordPress
* `-e WORDPRESS_DB_USER=mysql_user`: กำหนดชื่อผู้ใช้ MySQL ที่มีสิทธิ์ในการเข้าถึงฐานข้อมูล WordPress
* `-e WORDPRESS_DB_PASSWORD=mysql_password`: กำหนดรหัสผ่านสำหรับผู้ใช้ MySQL
* `-e WORDPRESS_DB_NAME=mysql_database`: กำหนดชื่อฐานข้อมูล MySQL ที่ WordPress จะใช้
* `-p 8080:80`: แมพพอร์ต 8080 ของโฮสต์ไปยังพอร์ต 80 ของ container (หากพอร์ต 80 ถูกใช้โดยโปรแกรมอื่นบนเครื่อง คุณสามารถเปลี่ยนเป็นพอร์ตอื่นได้)
* `wordpress`: เรียกใช้ Docker Image ที่ใช้สำหรับการสร้าง container

### Step 3: Manage Your Docker Container
Here are some useful Docker commands for managing your container:

* View running containers:
```sh
docker ps
```

* View container logs:
```sh
docker logs wordpress-container
```

* Stop the container:
```sh
docker stop wordpress-container
```

* Start the container:
```sh
docker start wordpress-container
```

* Remove the container:
```sh
docker rm wordpress-container
```

* Access to the container:
```sh
docker exec -it wordpress-container /bin/bash
```

ตัวอย่างนี้จะสร้าง container ของ WordPress และเชื่อมต่อกับ MySQL database ที่ระบุ และ WordPress จะเริ่มทำงานที่พอร์ต 8080 ของเครื่องโฮสต์ คุณสามารถเข้าถึงเว็บไซต์ WordPress โดยเข้าที่ `http://localhost:8080` ในเว็บเบราว์เซอร์ของคุณ