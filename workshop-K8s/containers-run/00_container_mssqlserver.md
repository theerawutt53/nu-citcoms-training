
# Microsoft SQL Server Docker Container

This project demonstrates how to run an Microsoft SQL Server using Docker.

## Prerequisites

- Docker installed on your machine. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Getting Started

### Step 1: Pull the Microsoft SQL Server  Docker Image

Open your terminal or command prompt and execute the following command to pull the latest Microsoft SQL Server image from Docker Hub:

```sh
docker pull mcr.microsoft.com/mssql/server:2019-latest
```

### Step 2: Run a Microsoft SQL Server Container:
Once the image is pulled, you can run a Microsoft SQL Server container. Use the following command to start the container with user `sa`:

```sh
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=YourPassword123!' -p 1433:1433 --name sqlserver -d mcr.microsoft.com/mssql/server:2019-latest
```

#### คำอธิบาย:

* `-e 'ACCEPT_EULA=Y'`: ยอมรับข้อตกลงการใช้งาน
* `-e 'SA_PASSWORD=YourPassword123!'`: ตั้งค่ารหัสผ่านสำหรับผู้ใช้ sa (ต้องมีความซับซ้อนเพียงพอ)
* `-p 1433:1433`: แมพ port 1433 บน host ไปที่ port 1433 ใน container
* `--name sqlserver`: ตั้งชื่อให้กับ container
* `-d`: รัน container ใน detached mode (ใน background)

### Step 3: Manage Your Docker Container
Here are some useful Docker commands for managing your container:

* View running containers:

```sh
docker ps
```


* View container logs:
```sh
docker logs Microsoft SQL Server-container
```

### Step 4: Connect to Microsoft SQL Server:

คุณสามารถเชื่อมต่อกับ MSSQL server ที่รันอยู่ใน Docker container ได้โดยใช้เครื่องมือ SQL client เช่น SQL Server Management Studio (SSMS) หรือ Azure Data Studio โดยใช้การตั้งค่าดังนี้:

* Server name: `localhost,1433`
* Authentication: `SQL Server Authentication`
* Login: `sa`
* Password: `YourPassword123!`