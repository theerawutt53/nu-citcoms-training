docker run -p 9000:9000 --name minio \
  -v /mnt/data:/data \
  -e "MINIO_ROOT_USER=YOURACCESSKEY" \
  -e "MINIO_ROOT_PASSWORD=YOURSECRETKEY" \
  minio/minio server /data