const JSONStream = require('JSONStream');
const through2 = require('through2');
const fs = require('fs'); 

fs.createReadStream('./json_file.json')
.pipe(JSONStream.parse('RECORDS.*'))
.pipe(through2.obj(function(chunk, enc, callback) {
    console.log(chunk);
    callback();
  });
})