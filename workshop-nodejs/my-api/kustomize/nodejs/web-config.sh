kubectl create secret generic web-secret -n workshop-nodejs \
  --from-literal=MONGODB_URL=mongodb://test:test34679@mongodb:27017 \
  --from-literal=ENDPOINT_MINIO=training-minio.nursenu.pro \
  --from-literal=ACCESSKEY_MINIO=root \
  --from-literal=SECRETKEY_MINIO=1212312121Hey \
  --from-file=./secrets/jwt_cert \
  --from-file=./secrets/jwt_key

