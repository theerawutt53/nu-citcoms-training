const crypto = require('crypto');

const passwordHash = (passwordUser, salt) => {
  let bytes = new Buffer(passwordUser || '', 'utf16le');
  let src = new Buffer(salt || '', 'base64');
  let dst = new Buffer(src.length + bytes.length);
  src.copy(dst, 0, 0, src.length);
  bytes.copy(dst, src.length, 0, bytes.length);
  return crypto.createHash('sha1').update(dst).digest('base64');
}

const passwordSalt = (passwordUser, salt) => {
  var passwordSalt = crypto.createHash('sha1').update(passwordUser).digest('base64');
  return passwordSalt;
}

module.exports = {
  passwordHash,
  passwordSalt,
}