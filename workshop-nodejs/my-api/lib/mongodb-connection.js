
const { MongoClient } = require('mongodb')

const connectionString = process.env.MONGODB_URL

const client = new MongoClient(connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

var clientConnection = null

module.exports = {
  connectToServer: async (cb) => {
    if (!clientConnection) {
      await client.connect()
      clientConnection = client
    }
  },
  getConnection: async (db) => {
    if (!clientConnection) {
      await client.connect()
      clientConnection = client
    }
    return clientConnection
  }
}

