
const uuid = require('uuid')
const express = require('express')
const router = express.Router()
const dbClient = require('./mongodb-connection')

router.get('/data/:id', async (req, res) => {
  const client = await dbClient.getConnection()
  const { id } = req.params
  const doc = await client.db(req.db)
    .collection(req.collection)
    .findOne({ _id: id })
  if (doc != null) {
    res.json(doc)
  } else {
    res.json({ 'ok': false, 'message': 'Not Found' })
  }
})

router.post('/data/:id?', async (req, res) => {
  let key = req.params.id ? req.params.id : ''
  key = (key == '') ? {} : { "_id": key }

  /*
  คำสั่ง key = (key == '') ? {} : { "_id": key } 
  เป็นการใช้เทคนิค Conditional (Ternary) Operator ใน JavaScript 
  เพื่อทำการตรวจสอบค่าและกำหนดค่าใหม่ให้กับตัวแปร key ตามเงื่อนไขที่กำหนด
  
  ตรวจสอบว่าตัวแปร key เป็นสตริงว่างหรือไม่ (มีค่าเป็น '' หรือไม่)
  ถ้าเงื่อนไขเป็นจริง (คือ key เป็นสตริงว่าง) ตัวแปร key จะถูกกำหนดค่าใหม่ให้เป็น object เปล่า
  
  ถ้าเงื่อนไขเป็นเท็จ (คือ key ไม่ใช่สตริงว่าง) 
  ตัวแปร key จะถูกกำหนดค่าใหม่ให้เป็นวัตถุที่มีฟิลด์ _id 
  โดยมีค่าของ _id เท่ากับค่าปัจจุบันของตัวแปร key
  */

  let value = req.body

  if (!key._id) {
    key['_id'] = uuid.v1().replace(/-/g, '')
    value['_id'] = key['_id']
  }

  value = { $set: value }

  const client = await dbClient.getConnection()
  const collection = await client.db(req.db).collection(req.collection)

  try {
    //console.log({key,value})
    let docs = await collection.updateOne(key, value, { 'upsert': true })
    docs = docs.value ? docs.value : value['$set']
    res.status(200).json({ ...docs, _id: key['_id'] })
  } catch (err) {
    console.log(err)
    res.status(500).json({ 'ok': false, 'message': err })
  }
})

router.post('/query', async (req, res) => {
  const { body } = req

  var limit = 0
  var skip = 0

  skip = body.skip ? parseInt(body.skip) : 0
  limit = body.limit ? parseInt(body.limit) : 0

  const client = await dbClient.getConnection()
  const collection = await client.db(req.db).collection(req.collection)
  
  try {
    if (body.count) {
      collection.count(body.count, (err, count) => {
        res.json(count)
      })
    } else if (body.aggregate) {
      const content = await collection.aggregate(body.aggregate).toArray()
      res.status(200).json(content)
    } else if (body.distinct) {
      collection.distinct(body.distinct, body.query).then((docs) => {
        res.json(docs)
      })
    } else {
      const { query, projection } = body
      const content = await collection.find(query, projection).skip(skip).limit(limit).toArray()
      res.status(200).json(content)
    }
  } catch (err) {
    console.log('error', err)
    res.status(500).json({ message: 'Error' })
  }
})

module.exports = router