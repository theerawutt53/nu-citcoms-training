const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const fs = require('fs')

const dbClient = require('./mongodb-connection')
const encryption = require('./encryption');

const jwtOptions = {
  jwtCert: fs.readFileSync(process.env.JWT_CERT),
  jwtKey: fs.readFileSync(process.env.JWT_KEY),
};

router.post('/login', async function (req, res, next) {
  const client = await dbClient.getConnection()
  let { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({ ok: false, message: "bad request" });
  }
  username = username.trim();
  username = username.toLowerCase();
  password = password.trim();

  const passwordSalt = encryption.passwordSalt(password);
  const passwordHash = encryption.passwordHash(password, passwordSalt);

  let user = await client.db("citcoms").collection("user").findOne({
    user: username,
    salt: passwordSalt,
    hash: passwordHash,
  });

  if (!user) {
    return res
      .status(401)
      .json({ ok: false, message: "username หรือ Password ไม่ถูกต้อง" });
  }

  // login pass
  let collectionRoles = await client
    .db("citcoms")
    .collection("role")
    .findOne({ user: username });

  if (!collectionRoles) {
    return res
      .status(401)
      .json({ ok: false, message: "username หรือ Password ไม่ถูกต้อง" });
  }

  if (collectionRoles?.disable) {
    return res
      .status(401)
      .send({ ok: false, message: "บัญชีของคุณถูกระงับการใช้" });
  }

  const payloads = {
    roles: collectionRoles.roles,
    user: collectionRoles.user,
  };

  const token = jwt.sign(payloads, jwtOptions.jwtKey, {
    algorithm: "RS256",
    expiresIn: "14d",
  });

  return res.status(200).json({ user: collectionRoles, token: token });
})

module.exports = router                                                                                                                              
