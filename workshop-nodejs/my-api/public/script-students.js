document.addEventListener('DOMContentLoaded', () => {
    const tableBody = document.getElementById('data-table').getElementsByTagName('tbody')[0];

    // const prevButton = document.getElementById('prev');
    // const nextButton = document.getElementById('next');
    // const pageInfo = document.getElementById('page-info');

    // let currentPage = 1;
    // const rowsPerPage = 10;
    // let totalRows = 0;

    // Function to fetch data
    async function fetchData() {
        try {
            const response = await fetch('http://localhost:3000/db/citcoms/students/query', {
                method: 'POST',
                body: { "query": { "studentID": "67282846" } }
            });

            if (!response.ok) {
                throw new Error('Upload failed.');
            }
            const data = await response.json();
            console.log(data)

            totalRows = data.length;
            displayPage(data);
            // displayPage(data, currentPage);

        } catch (error) {
            console.error('Error:', error);
        }
    }

    // Function to populate table with data
    function displayPage(data) {
        data.forEach(user => {
            const row = tableBody.insertRow();
            const cellId = row.insertCell(0);
            const cellName = row.insertCell(1);
            const cellEmail = row.insertCell(2);

            cellId.textContent = user.studentID;
            cellName.textContent = `${user.firstName} ${user.lastName}`;
            cellEmail.textContent = user.age;
        });
    }

    // function displayPage(data, page) {
    //     tableBody.innerHTML = '';
    //     const start = (page - 1) * rowsPerPage;
    //     const end = start + rowsPerPage;
    //     const paginatedData = data.slice(start, end);

    //     paginatedData.forEach(user => {
    //         const row = tableBody.insertRow();
    //         row.insertCell(0).textContent = user.studentID;
    //         row.insertCell(1).textContent = `${user.firstName} ${user.lastName}`;
    //         row.insertCell(2).textContent = user.age;
    //     });

    //     pageInfo.textContent = `Page ${page} of ${Math.ceil(totalRows / rowsPerPage)}`;
    //     prevButton.disabled = page === 1;
    //     nextButton.disabled = page === Math.ceil(totalRows / rowsPerPage);
    // }

    // prevButton.addEventListener('click', () => {
    //     if (currentPage > 1) {
    //         currentPage--;
    //         fetchData();
    //     }
    // });

    // nextButton.addEventListener('click', () => {
    //     if (currentPage < Math.ceil(totalRows / rowsPerPage)) {
    //         currentPage++;
    //         fetchData();
    //     }
    // });

    // Fetch data when the page loads
    fetchData();
});
