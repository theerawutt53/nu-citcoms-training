document.addEventListener('DOMContentLoaded', () => {
  const tableBody = document.getElementById('data-table').getElementsByTagName('tbody')[0];

  // Function to fetch data
  async function fetchData() {
      try {
          const response = await fetch('http://localhost:3000/documents');
          // const response = await fetch('/documents');
          if (!response.ok) {
              throw new Error('Network response was not ok ' + response.statusText);
          }
          const data = await response.json();
          console.log(data)
          populateTable(data);
      } catch (error) {
          console.error('There was a problem with the fetch operation:', error);
      }
  }

  // Function to populate table with data
  function populateTable(data) {
      data.forEach(user => {
          const row = tableBody.insertRow();
          const cellId = row.insertCell(0);
          const cellName = row.insertCell(1);
          const cellEmail = row.insertCell(2);

          cellId.textContent = user._id;
          cellName.textContent = `${user.firstname} ${user.firstname}`;
          cellEmail.textContent = user.email;
      });
  }

  // Fetch data when the page loads
  fetchData();
});
