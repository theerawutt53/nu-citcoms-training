const { MongoClient } = require('mongodb')
const uuid = require('uuid')

const connectionString = 'mongodb://root:Nuadmin12345@45.77.36.236:27017'

// Function to generate random integer within a range
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function to generate a random name
function generateRandomName() {
  const names = [
    "สมชาย", "สมศักดิ์", "อนุวัฒน์", "อภิรักษ์", "กิตติพงษ์", "สรยุทธ", "ณัฐพงษ์", "สุริยา", "ภัทรพล", "วรพล",
    "พรชัย", "ชัยวัฒน์", "ธนภัทร", "ณรงค์", "โสภณ", "สุพจน์", "วิชัย", "ชัชวาลย์", "ประเสริฐ", "รุ่งโรจน์",
    "ฐิติพันธ์", "อาทิตย์", "เอกชัย", "กฤษฎา", "ปรีชา", "มานพ", "ธีรภัทร์", "วันชัย", "ประพัฒน์", "ศักดา",
    "ธรรมรงค์", "ชลวิทย์", "คชรัตน์", "วิรัช", "สุชาติ", "สุเทพ", "ยุทธนา", "พิสิฐ", "อดุลย์", "ชัชวาลย์",
    "ภาวัต", "บุญธรรม", "จุฑามาศ", "พลอยไพลิน", "ปริศนา", "ณัฐชา", "อนัญญา", "นลินี", "สุภาภรณ์", "สุพัตรา",
    "วรรณภา", "จารุวรรณ", "ขวัญใจ", "สิริพร", "นันทนา", "สรินยา", "พิศมัย", "ปัทมา", "ศุภลักษณ์", "นิภาพร"
  ];
  return names[getRandomInt(0, names.length - 1)];
}

// Function to generate a random name
function generateRandomLastName() {
  const lastnames = [
    "สุวรรณภูมิ", "โพธิราช", "สุริยวงศ์", "อมาตยกุล", "น้ำเสนา", "อุดมลาภ", "รัตนสุข", 
    "สิริมงคล", "บุนนาค", "เทพพิทักษ์", "เนาวรัตน์", "ศิริกุล", "นิมมานเหมินท์", "อภิชาติ", 
    "ไชยวรรณ", "ศิริพัฒน์", "จินดารัตน์", "พูนผล", "สุกนต์รัตน์", "จันทนายิ่งยง", 
    "กิตติสมภพ", "เจริญวงศ์", "คงสกุล", "คำพนารัตน์", "บุญส่ง", "ชาติจรัส", "วชิราภา", 
    "แสงภักดี", "พรหมจรรย์", "พงศ์ภัค", "ธรรมสาส์น", "ธนโชติ", "สุรศักดิ์"
  ];
  return lastnames[getRandomInt(0, lastnames.length - 1)];
}

// Function to generate random persons
function generateRandomUsers(count) {
  const persons = [];
  for (let i = 1; i <= count; i++) {
    const user = {
      _id: uuid.v1().replace(/-/g, ''),
      firstName: `${generateRandomName()}${getRandomInt(1, 100)}`,
      lastName: `${generateRandomLastName()}${getRandomInt(1, 100)}`,
      age: getRandomInt(18, 60)
    };
    persons.push(user);
  }
  return persons;
}

const run = async () => {
  const client = new MongoClient(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  try {
    await client.connect();
    const db = client.db('citcoms');
    const collection = db.collection('persons');

    const persons = generateRandomUsers(10000);

    const result = await collection.insertMany(persons);
    console.log(`${result.insertedCount} documents were inserted`);

    const docs = await client
      .db('citcoms')
      .collection('persons')
      .find({})
      //.limit(100)
    const total = await docs.count()
    var count = 0
    for await (const doc of docs) {
      count++
      console.log(`${count}/${total}|${doc._id}`)
    }

  } finally {
    await client.close()
  }
}

run()