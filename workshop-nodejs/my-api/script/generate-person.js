const fs = require('fs');

// Function to generate random integer within a range
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function to generate a random name
function generateRandomName() {
  const names = ['Alice', 'Bob', 'Charlie', 'David', 'Emma', 'Frank', 'Grace', 'Henry', 'Ivy', 'Jack'];
  return names[getRandomInt(0, names.length - 1)];
}

// Function to generate a random name
function generateRandomLastName() {
  const names = ['Papil', 'Bobrager', 'Leddi', 'DaviJohnNazzy', 'EmmaMaduel', 'Frankkin', 'BewGrace', 'PapiHenry', 'IvyRocher', 'JackDanial'];
  return names[getRandomInt(0, names.length - 1)];
}

// Function to generate random persons
function generateRandomUsers(count) {
  const persons = [];
  for (let i = 1; i <= count; i++) {
    const user = {
      id: i,
      firstName: `${generateRandomName()}${getRandomInt(1, 100)}`,
      lastName: `${generateRandomLastName()}${getRandomInt(1, 100)}`,
      age: getRandomInt(18, 60)
    };
    persons.push(user);
  }
  return persons;
}

// Generate 1000 random persons
const persons = generateRandomUsers(100000);

// Write persons array to persons.json file
fs.writeFile('./persons.json', JSON.stringify(persons, null, 2), (err) => {
  if (err) {
    console.error('Error writing file:', err);
    return;
  }
  console.log('Users data has been written to persons.json');
});
