const express = require('express')
const cors = require('cors')
const dbClient = require('./lib/mongodb-connection')
const ensureLogin = require('./lib/ensure-login')
// const ensureLogin = require('./lib/ensure-login')

const app = express()

const port = process.env.PORT || 3000;

app.use(cors())
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ extended: true }))

app.get('/health', (req, res) => res.send('check server health'))

app.use('/auth', require('./lib/auth'));

app.param('db', async (req, res, next, value) => {
  req.db = value
  next()
})

app.param('collection', (req, res, next, value) => {
  req.collection = value
  next()
})

app.use('/db/:db/:collection',
  ensureLogin,
  require('./lib/mongodb-api'))

// app.use('/db/:db/:collection', ensureLogin,
//   require('./lib/authorization'),
//   require('./lib/mongodb'))

// app.use('/auth', require('./lib/auth'))

app.get('/servertime', (req, res) => {
  var long_date = new Date().getTime()
  res.send(long_date.toString());
})

require('express-readme')(app, {
  filename: './README.md',
  routes: ['/', '/readme']
});

const run = async () => {
  await dbClient.connectToServer()
  app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
  })
}

run()
