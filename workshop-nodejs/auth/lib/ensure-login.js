const jwt = require('jsonwebtoken');
const fs = require('fs')

const jwtOptions = {
  jwtCert: fs.readFileSync(process.env.JWT_CERT),
  jwtKey: fs.readFileSync(process.env.JWT_KEY),
};

const ensureLogin = (req, res, next) => {
  // console.log(req.headers)
  try {
    if (!req.headers["authorization"]) return res.sendStatus(401)

    const token = req.headers["authorization"].replace("JWT ", "")
    // console.log(token)
    jwt.verify(token, jwtOptions.jwtKey, { algorithms: ["RS256"] }, (err, decoded) => {
      console.log(decoded)
      if (err) throw new Error(error)
    })
    next()
  } catch (error) {
    return res.sendStatus(401)
  }
}

module.exports = ensureLogin