const encryption = require('./encryption');

const password = '1234qwer'
const passwordSalt = encryption.passwordSalt(password);
const passwordHash = encryption.passwordHash(password, passwordSalt);

console.log(`password ${password}`)
console.log(`password salt ${passwordSalt}`)
console.log(`password hash ${passwordHash}`)