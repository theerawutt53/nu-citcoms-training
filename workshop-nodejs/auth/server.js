const express = require('express')
const cors = require('cors')
const dbClient = require('./lib/mongodb-connection')
const ensureLogin = require('./lib/ensure-login')

const app = express()

const port = process.env.PORT || 3000;

app.use(cors())
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ extended: true }))


app.get('/health', (req, res) => res.send('check server health'))

app.use('/auth', require('./lib/auth'));

app.get('/servertime', ensureLogin, (req, res) => {
  var long_date = new Date().getTime()
  res.send(long_date.toString());
})

const run = async () => {
  await dbClient.connectToServer()
  app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
  })
}

run()
