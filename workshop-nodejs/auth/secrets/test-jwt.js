const fs = require('fs');
const jwt = require('jsonwebtoken');

const jwtCert = fs.readFileSync('./jwt.crt')
const jwtKey = fs.readFileSync('./jwt.key')


// Payload to sign
const payload = {
  id: '1234567890',
  name: 'Theerawut Thaweephattharawong',
  email: 'theerawutt53@gmail.com'
};

// Sign the JWT
const token = jwt.sign(payload, jwtKey, { algorithm: 'RS256' });

console.log('JWT:', token);

// Verify the JWT
jwt.verify(token, jwtCert, { algorithms: ['RS256'] }, (err, decoded) => {
  if (err) {
    console.error('JWT verification failed:', err);
  } else {
    console.log('JWT verified successfully:', decoded);
  }
});
